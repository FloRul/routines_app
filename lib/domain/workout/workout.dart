import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:routines_app/domain/core/failures.dart';
import 'package:routines_app/domain/core/value_objects.dart';
import 'package:routines_app/domain/core/value_validators.dart';
part 'workout.freezed.dart';

@freezed
class Workout with _$Workout {
  const Workout._();

  const factory Workout({
    required UniqueId id,
    required Name name,
    required FiniteList<Exercise> exercises,
  }) = _Workout;

  factory Workout.empty() => Workout(
        id: UniqueId(),
        name: Name(''),
        exercises: FiniteList(emptyList()),
      );

  Option<ValueFailure<dynamic>> get failureOption {
    return name.failureOrUnit
        .andThen(exercises
            .getOrThrow()
            .map((e) => e.failureOption)
            .filter((failureOption) => failureOption.isSome())
            .getOrElse(0, (_) => none())
            .fold(() => right(unit), (f) => left(f)))
        .fold((f) => some(f), (_) => none());
  }
}

@freezed
class Exercise with _$Exercise {
  const Exercise._();

  const factory Exercise({
    required UniqueId id,
    required Name name,
    required Volume volume,
    required RestDuration restDuration,
  }) = _Exercise;

  factory Exercise.empty() => Exercise(
        id: UniqueId(),
        name: Name(''),
        volume: Volume(left(0)),
        restDuration: RestDuration(0),
      );

  Option<ValueFailure<dynamic>> get failureOption {
    return name.failureOrUnit
        .andThen(volume.failureOrUnit)
        .andThen(restDuration.failureOrUnit)
        .fold(
          (f) => some(f),
          (_) => none(),
        );
  }
}

class Name extends ValueObject<String> {
  static const maxLength = 30;

  @override
  final Either<ValueFailure<String>, String> value;

  factory Name(String input) {
    return Name._(validateMaxStringLength(input, maxLength)
        .flatMap(validateStringNotEmpty));
  }

  const Name._(this.value);
}

// Left is duration, right is repetition
class Volume extends ValueObject<Either<int, int>> {
  @override
  final Either<ValueFailure<Either<int, int>>, Either<int, int>> value;

  factory Volume(Either<int, int> input) {
    return Volume._(validateVolume(input));
  }

  const Volume._(this.value);

  factory Volume.fromString(String input) {
    switch (input[0]) {
      case 'r':
        return Volume(right(int.parse(input.substring(1))));
      case 'd':
        return Volume(left(int.parse(input.substring(1))));
      default:
        return Volume(right(0));
    }
  }
}

class RestDuration extends ValueObject<int> {
  @override
  final Either<ValueFailure<int>, int> value;
  factory RestDuration(int input) {
    return RestDuration._(validateNonNegativeInt(input));
  }

  const RestDuration._(this.value);
}

class FiniteList<T> extends ValueObject<KtList<T>> {
  @override
  final Either<ValueFailure<KtList<T>>, KtList<T>> value;

  static const maxLength = 6;

  factory FiniteList(KtList<T> input) {
    return FiniteList._(validateMaxListLength(input, maxLength));
  }
  const FiniteList._(this.value);

  int get length => value.getOrElse(() => emptyList()).size;

  bool get isFull => length == maxLength;
}

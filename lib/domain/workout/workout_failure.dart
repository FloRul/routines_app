import 'package:freezed_annotation/freezed_annotation.dart';

part 'workout_failure.freezed.dart';

@freezed
class WorkoutFailure with _$WorkoutFailure {
  const factory WorkoutFailure.unexpected() = _Unexpected;
  const factory WorkoutFailure.insufficientPermission() =
      _InsufficientPermission;
  const factory WorkoutFailure.unableToUpdate() = _UnableToUpdate;
}

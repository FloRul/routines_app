import 'package:dartz/dartz.dart';
import 'package:kt_dart/collection.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:routines_app/domain/workout/workout_failure.dart';

abstract class IWorkoutRepository {
  Stream<Either<WorkoutFailure, KtList<Workout>>> watchAll();
  Future<Either<WorkoutFailure, Unit>> create(Workout workout);
  Future<Either<WorkoutFailure, Unit>> update(Workout workout);
  Future<Either<WorkoutFailure, Unit>> delete(Workout workout);
}

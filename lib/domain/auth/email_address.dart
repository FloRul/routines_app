import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:routines_app/domain/core/failures.dart';
import 'package:routines_app/domain/core/value_objects.dart';
import 'package:routines_app/domain/core/value_validators.dart';

@immutable
class EmailAddress extends ValueObject<String> {
  @override
  final Either<ValueFailure<String>, String> value;

  factory EmailAddress(String input) {
    return EmailAddress._(
      validateEmailAddress(input),
    );
  }
  const EmailAddress._(this.value);
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:routines_app/domain/auth/user.dart' as domain;
import 'package:routines_app/domain/core/value_objects.dart';

extension FirebaseUserDomainX on User {
  domain.User toDomain() {
    return domain.User(id: UniqueId.fromUniqueString(uid));
  }
}

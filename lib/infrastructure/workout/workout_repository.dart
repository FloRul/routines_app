import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kt_dart/kt.dart';
import 'package:rxdart/rxdart.dart';
import 'package:routines_app/domain/workout/workout_failure.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:routines_app/infrastructure/workout/workout_dto.dart';

class WorkoutRepository {
  DocumentReference<Object?> userDocuments() => FirebaseFirestore.instance
      .collection('users')
      .doc(FirebaseAuth.instance.currentUser!.uid);

  Future<Either<WorkoutFailure, Unit>> create(
      Workout workout, User user) async {
    try {
      final userDoc = userDocuments();
      final workoutDto = WorkoutDto.fromDomain(workout);

      await userDoc.workoutCollection
          .doc(workoutDto.id)
          .set(workoutDto.toJson());

      return right(unit);
    } on FirebaseException catch (e) {
      if (e.code.contains('PERMISSION_DENIED')) {
        return left(const WorkoutFailure.insufficientPermission());
      } else {
        return left(const WorkoutFailure.unexpected());
      }
    }
  }

  Future<Either<WorkoutFailure, Unit>> delete(Workout workout) async {
    try {
      final userDoc = userDocuments();
      final workoutDto = WorkoutDto.fromDomain(workout);

      await userDoc.workoutCollection.doc(workoutDto.id).delete();

      return right(unit);
    } on FirebaseException catch (e) {
      if (e.code.toUpperCase().contains('PERMISSION-DENIED')) {
        return left(const WorkoutFailure.insufficientPermission());
      } else {
        return left(const WorkoutFailure.unexpected());
      }
    }
  }

  Future<Either<WorkoutFailure, Unit>> update(Workout workout) async {
    try {
      final userDoc = userDocuments();
      final workoutDto = WorkoutDto.fromDomain(workout);
      await userDoc.workoutCollection
          .doc(workoutDto.id)
          .update(workoutDto.toJson());

      return right(unit);
    } on FirebaseException catch (e) {
      if (e.code.contains('PERMISSION_DENIED')) {
        return left(const WorkoutFailure.insufficientPermission());
      } else if (e.code.contains('NOT_FOUND')) {
        return left(const WorkoutFailure.unableToUpdate());
      } else {
        return left(const WorkoutFailure.unexpected());
      }
    }
  }

  Stream<Either<WorkoutFailure, KtList<Workout>>> watchAll() async* {
    final userDoc = userDocuments();
    yield* userDoc.workoutCollection
        .snapshots()
        .map(
          (snapshot) => right<WorkoutFailure, KtList<Workout>>(
            snapshot.docs
                .map((doc) => WorkoutDto.fromFirestore(doc).toDomain())
                .toImmutableList(),
          ),
        )
        .onErrorReturnWith((error, stackTrace) {
      if (error is FirebaseException &&
          error.code.toUpperCase().contains('PERMISSION-DENIED')) {
        return left(const WorkoutFailure.insufficientPermission());
      } else {
        // log.error(error.toString());
        return left(const WorkoutFailure.unexpected());
      }
    });
  }
}

extension DocumentReferenceX on DocumentReference {
  CollectionReference get workoutCollection => collection('workouts');
}

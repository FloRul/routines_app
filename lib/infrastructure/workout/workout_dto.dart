import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:routines_app/domain/core/value_objects.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:kt_dart/kt.dart';
part 'workout_dto.freezed.dart';
part 'workout_dto.g.dart';

@freezed
class WorkoutDto with _$WorkoutDto {
  const WorkoutDto._();

  const factory WorkoutDto({
    @JsonKey(ignore: true) @Default('') String id,
    required String name,
    required List<ExerciseDto> exercises,
  }) = _WorkoutDto;

  factory WorkoutDto.fromDomain(Workout workout) {
    return WorkoutDto(
      id: workout.id.getOrThrow(),
      name: workout.name.getOrThrow(),
      exercises: workout.exercises
          .getOrThrow()
          .map((e) => ExerciseDto(
              id: e.id.getOrThrow(),
              name: e.name.getOrThrow(),
              volume: e.volume.getOrThrow().fold((d) => 'd$d', (r) => 'r$r'),
              restDuration: e.restDuration.getOrThrow()))
          .asList(),
    );
  }

  Workout toDomain() {
    return Workout(
      id: UniqueId.fromUniqueString(id),
      name: Name(name),
      exercises:
          FiniteList(exercises.map((dto) => dto.toDomain()).toImmutableList()),
    );
  }

  factory WorkoutDto.fromJson(Map<String, dynamic> json) =>
      _$WorkoutDtoFromJson(json);

  factory WorkoutDto.fromFirestore(DocumentSnapshot document) {
    return WorkoutDto.fromJson(document.data() as Map<String, dynamic>)
        .copyWith(id: document.id);
  }
}

@freezed
class ExerciseDto with _$ExerciseDto {
  const ExerciseDto._();

  const factory ExerciseDto({
    required String id,
    required String name,
    required String volume,
    required int restDuration,
  }) = _ExerciseDto;

  factory ExerciseDto.fromDomain(Exercise exercise) {
    return ExerciseDto(
        id: exercise.id.getOrThrow(),
        name: exercise.name.getOrThrow(),
        volume: exercise.volume
            .getOrThrow()
            .fold((rep) => 'r$rep', (duration) => 'd$duration'),
        restDuration: exercise.restDuration.getOrThrow());
  }

  factory ExerciseDto.fromJson(Map<String, dynamic> json) =>
      _$ExerciseDtoFromJson(json);

  Exercise toDomain() {
    return Exercise(
        id: UniqueId.fromUniqueString(id),
        name: Name(name),
        volume: Volume.fromString(volume),
        restDuration: RestDuration(restDuration));
  }
}

// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'workout_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

WorkoutDto _$WorkoutDtoFromJson(Map<String, dynamic> json) {
  return _WorkoutDto.fromJson(json);
}

/// @nodoc
class _$WorkoutDtoTearOff {
  const _$WorkoutDtoTearOff();

  _WorkoutDto call(
      {@JsonKey(ignore: true) String id = '',
      required String name,
      required List<ExerciseDto> exercises}) {
    return _WorkoutDto(
      id: id,
      name: name,
      exercises: exercises,
    );
  }

  WorkoutDto fromJson(Map<String, Object?> json) {
    return WorkoutDto.fromJson(json);
  }
}

/// @nodoc
const $WorkoutDto = _$WorkoutDtoTearOff();

/// @nodoc
mixin _$WorkoutDto {
  @JsonKey(ignore: true)
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  List<ExerciseDto> get exercises => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $WorkoutDtoCopyWith<WorkoutDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkoutDtoCopyWith<$Res> {
  factory $WorkoutDtoCopyWith(
          WorkoutDto value, $Res Function(WorkoutDto) then) =
      _$WorkoutDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(ignore: true) String id,
      String name,
      List<ExerciseDto> exercises});
}

/// @nodoc
class _$WorkoutDtoCopyWithImpl<$Res> implements $WorkoutDtoCopyWith<$Res> {
  _$WorkoutDtoCopyWithImpl(this._value, this._then);

  final WorkoutDto _value;
  // ignore: unused_field
  final $Res Function(WorkoutDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? exercises = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      exercises: exercises == freezed
          ? _value.exercises
          : exercises // ignore: cast_nullable_to_non_nullable
              as List<ExerciseDto>,
    ));
  }
}

/// @nodoc
abstract class _$WorkoutDtoCopyWith<$Res> implements $WorkoutDtoCopyWith<$Res> {
  factory _$WorkoutDtoCopyWith(
          _WorkoutDto value, $Res Function(_WorkoutDto) then) =
      __$WorkoutDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(ignore: true) String id,
      String name,
      List<ExerciseDto> exercises});
}

/// @nodoc
class __$WorkoutDtoCopyWithImpl<$Res> extends _$WorkoutDtoCopyWithImpl<$Res>
    implements _$WorkoutDtoCopyWith<$Res> {
  __$WorkoutDtoCopyWithImpl(
      _WorkoutDto _value, $Res Function(_WorkoutDto) _then)
      : super(_value, (v) => _then(v as _WorkoutDto));

  @override
  _WorkoutDto get _value => super._value as _WorkoutDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? exercises = freezed,
  }) {
    return _then(_WorkoutDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      exercises: exercises == freezed
          ? _value.exercises
          : exercises // ignore: cast_nullable_to_non_nullable
              as List<ExerciseDto>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_WorkoutDto extends _WorkoutDto {
  const _$_WorkoutDto(
      {@JsonKey(ignore: true) this.id = '',
      required this.name,
      required this.exercises})
      : super._();

  factory _$_WorkoutDto.fromJson(Map<String, dynamic> json) =>
      _$$_WorkoutDtoFromJson(json);

  @override
  @JsonKey(ignore: true)
  final String id;
  @override
  final String name;
  @override
  final List<ExerciseDto> exercises;

  @override
  String toString() {
    return 'WorkoutDto(id: $id, name: $name, exercises: $exercises)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _WorkoutDto &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            const DeepCollectionEquality().equals(other.exercises, exercises));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, id, name, const DeepCollectionEquality().hash(exercises));

  @JsonKey(ignore: true)
  @override
  _$WorkoutDtoCopyWith<_WorkoutDto> get copyWith =>
      __$WorkoutDtoCopyWithImpl<_WorkoutDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_WorkoutDtoToJson(this);
  }
}

abstract class _WorkoutDto extends WorkoutDto {
  const factory _WorkoutDto(
      {@JsonKey(ignore: true) String id,
      required String name,
      required List<ExerciseDto> exercises}) = _$_WorkoutDto;
  const _WorkoutDto._() : super._();

  factory _WorkoutDto.fromJson(Map<String, dynamic> json) =
      _$_WorkoutDto.fromJson;

  @override
  @JsonKey(ignore: true)
  String get id;
  @override
  String get name;
  @override
  List<ExerciseDto> get exercises;
  @override
  @JsonKey(ignore: true)
  _$WorkoutDtoCopyWith<_WorkoutDto> get copyWith =>
      throw _privateConstructorUsedError;
}

ExerciseDto _$ExerciseDtoFromJson(Map<String, dynamic> json) {
  return _ExerciseDto.fromJson(json);
}

/// @nodoc
class _$ExerciseDtoTearOff {
  const _$ExerciseDtoTearOff();

  _ExerciseDto call(
      {required String id,
      required String name,
      required String volume,
      required int restDuration}) {
    return _ExerciseDto(
      id: id,
      name: name,
      volume: volume,
      restDuration: restDuration,
    );
  }

  ExerciseDto fromJson(Map<String, Object?> json) {
    return ExerciseDto.fromJson(json);
  }
}

/// @nodoc
const $ExerciseDto = _$ExerciseDtoTearOff();

/// @nodoc
mixin _$ExerciseDto {
  String get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get volume => throw _privateConstructorUsedError;
  int get restDuration => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ExerciseDtoCopyWith<ExerciseDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExerciseDtoCopyWith<$Res> {
  factory $ExerciseDtoCopyWith(
          ExerciseDto value, $Res Function(ExerciseDto) then) =
      _$ExerciseDtoCopyWithImpl<$Res>;
  $Res call({String id, String name, String volume, int restDuration});
}

/// @nodoc
class _$ExerciseDtoCopyWithImpl<$Res> implements $ExerciseDtoCopyWith<$Res> {
  _$ExerciseDtoCopyWithImpl(this._value, this._then);

  final ExerciseDto _value;
  // ignore: unused_field
  final $Res Function(ExerciseDto) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? volume = freezed,
    Object? restDuration = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      volume: volume == freezed
          ? _value.volume
          : volume // ignore: cast_nullable_to_non_nullable
              as String,
      restDuration: restDuration == freezed
          ? _value.restDuration
          : restDuration // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$ExerciseDtoCopyWith<$Res>
    implements $ExerciseDtoCopyWith<$Res> {
  factory _$ExerciseDtoCopyWith(
          _ExerciseDto value, $Res Function(_ExerciseDto) then) =
      __$ExerciseDtoCopyWithImpl<$Res>;
  @override
  $Res call({String id, String name, String volume, int restDuration});
}

/// @nodoc
class __$ExerciseDtoCopyWithImpl<$Res> extends _$ExerciseDtoCopyWithImpl<$Res>
    implements _$ExerciseDtoCopyWith<$Res> {
  __$ExerciseDtoCopyWithImpl(
      _ExerciseDto _value, $Res Function(_ExerciseDto) _then)
      : super(_value, (v) => _then(v as _ExerciseDto));

  @override
  _ExerciseDto get _value => super._value as _ExerciseDto;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? volume = freezed,
    Object? restDuration = freezed,
  }) {
    return _then(_ExerciseDto(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as String,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      volume: volume == freezed
          ? _value.volume
          : volume // ignore: cast_nullable_to_non_nullable
              as String,
      restDuration: restDuration == freezed
          ? _value.restDuration
          : restDuration // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ExerciseDto extends _ExerciseDto {
  const _$_ExerciseDto(
      {required this.id,
      required this.name,
      required this.volume,
      required this.restDuration})
      : super._();

  factory _$_ExerciseDto.fromJson(Map<String, dynamic> json) =>
      _$$_ExerciseDtoFromJson(json);

  @override
  final String id;
  @override
  final String name;
  @override
  final String volume;
  @override
  final int restDuration;

  @override
  String toString() {
    return 'ExerciseDto(id: $id, name: $name, volume: $volume, restDuration: $restDuration)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ExerciseDto &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.volume, volume) || other.volume == volume) &&
            (identical(other.restDuration, restDuration) ||
                other.restDuration == restDuration));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, name, volume, restDuration);

  @JsonKey(ignore: true)
  @override
  _$ExerciseDtoCopyWith<_ExerciseDto> get copyWith =>
      __$ExerciseDtoCopyWithImpl<_ExerciseDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ExerciseDtoToJson(this);
  }
}

abstract class _ExerciseDto extends ExerciseDto {
  const factory _ExerciseDto(
      {required String id,
      required String name,
      required String volume,
      required int restDuration}) = _$_ExerciseDto;
  const _ExerciseDto._() : super._();

  factory _ExerciseDto.fromJson(Map<String, dynamic> json) =
      _$_ExerciseDto.fromJson;

  @override
  String get id;
  @override
  String get name;
  @override
  String get volume;
  @override
  int get restDuration;
  @override
  @JsonKey(ignore: true)
  _$ExerciseDtoCopyWith<_ExerciseDto> get copyWith =>
      throw _privateConstructorUsedError;
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'workout_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_WorkoutDto _$$_WorkoutDtoFromJson(Map<String, dynamic> json) =>
    _$_WorkoutDto(
      name: json['name'] as String,
      exercises: (json['exercises'] as List<dynamic>)
          .map((e) => ExerciseDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_WorkoutDtoToJson(_$_WorkoutDto instance) =>
    <String, dynamic>{
      'name': instance.name,
      'exercises': instance.exercises.map((e) => e.toJson()).toList(),
    };

_$_ExerciseDto _$$_ExerciseDtoFromJson(Map<String, dynamic> json) =>
    _$_ExerciseDto(
      id: json['id'] as String,
      name: json['name'] as String,
      volume: json['volume'] as String,
      restDuration: json['restDuration'] as int,
    );

Map<String, dynamic> _$$_ExerciseDtoToJson(_$_ExerciseDto instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'volume': instance.volume,
      'restDuration': instance.restDuration,
    };

import 'package:another_flushbar/flushbar.dart';
import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/application/auth/sign_in_form/sign_in_form_provider.dart';
import 'package:routines_app/domain/auth/auth_failure.dart';
import 'package:routines_app/application/auth/sign_in_form/sign_in_form_state.dart';
import 'package:routines_app/presentation/pages/initial_page.dart';
import 'package:routines_app/presentation/routes/router.gr.dart';

class SignInForm extends ConsumerWidget {
  const SignInForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen<SignInFormState>(
      signInStateProvider,
      (before, after) => after.authFailureOrSuccessOption.fold(
        () {},
        (either) => either.fold(
          (failure) => {
            Flushbar(
              message: failure.map(
                  cancelledByUser: (CancelledByUser value) => 'cancelledByUser',
                  emailAlreadyInUse: (EmailAlreadyInUse value) =>
                      'emailAlreadyInUse',
                  invalidEmailAndPasswordCombination:
                      (InvalidEmailAndPasswordCombination value) =>
                          'invalidEmailAndPasswordCombination',
                  serverError: (ServerError value) => 'serverError'),
            ).show(context),
          },
          (_) async {
            AutoRouter.of(context).replace(const DashboardRoute());
            ref.refresh(signedUserProvider);
          },
        ),
      ),
    );

    final formState = ref.watch(signInStateProvider);
    final notifier = ref.watch(signInStateProvider.notifier);
    return Form(
      autovalidateMode: formState.showErrorMessage
          ? AutovalidateMode.always
          : AutovalidateMode.disabled,
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        children: [
          const Text(
            '📝',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 130),
          ),
          const SizedBox(height: 20),
          TextFormField(
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.email),
              labelText: 'email'.tr(),
            ),
            autocorrect: false,
            onChanged: (value) => notifier.email = value,
            validator: (_) => formState.emailAddress.value.fold(
              (f) => f.maybeMap(
                  invalidEmail: (_) => 'invalidEmail'.tr(), orElse: () => null),
              (_) => null,
            ),
          ),
          const SizedBox(height: 20),
          TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.lock), labelText: 'password'.tr()),
            autocorrect: false,
            obscureText: true,
            onChanged: (value) => notifier.password = value,
            validator: (_) => formState.password.value.fold(
              (f) => f.maybeMap(
                  shortPassword: (_) => 'passwordTooShort'.tr(),
                  orElse: () => null),
              (_) => null,
            ),
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all<Color>(
                        Theme.of(context).primaryColor)),
                onPressed: () => notifier.signInWithEmailAndPassword(ref.read),
                child: Text(
                  'signIn'.tr(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
              ElevatedButton(
                style: ButtonStyle(
                    foregroundColor: MaterialStateProperty.all<Color>(
                        Theme.of(context).primaryColor)),
                onPressed: () =>
                    notifier.registerWithEmailAndPassword(ref.read),
                child: Text(
                  'register'.tr(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          ElevatedButton(
            style: ButtonStyle(
                foregroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).canvasColor)),
            onPressed: () => notifier.signInWithGoogle(ref.read),
            child: Text(
              'signInWithGoogle'.tr(),
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          if (formState.isSubmitting) ...[
            const SizedBox(
              height: 20,
            ),
            const LinearProgressIndicator(),
          ]
        ],
      ),
    );
  }
}

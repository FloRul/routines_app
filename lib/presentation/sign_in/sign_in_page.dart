import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:routines_app/presentation/sign_in/widgets/sign_in_form.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('signIn'.tr()),
      ),
      body: const SignInForm(),
    );
  }
}

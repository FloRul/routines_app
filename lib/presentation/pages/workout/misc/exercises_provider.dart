import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kt_dart/kt.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:routines_app/presentation/misc/exercises_presentation_classes.dart';

final exercisesPrimitiveProvider =
    StateNotifierProvider<FormExercises, KtList<ExercisePrimitive>>(
        (ref) => FormExercises(FiniteList(emptyList<Exercise>())));

class FormExercises extends StateNotifier<KtList<ExercisePrimitive>> {
  FormExercises([FiniteList<Exercise>? initialExercises])
      : super(emptyList<ExercisePrimitive>());

  set exercises(value) => state = value;

  void editName(int index, String value) {
    state = [
      for (final ex in state.iter)
        if (state.indexOf(ex) == index) ex.copyWith(name: value) else ex,
    ].toImmutableList();
  }

  void add(ExercisePrimitive ex) {
    state = state.plusElement(ex);
  }
}

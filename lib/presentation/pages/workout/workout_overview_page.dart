import 'package:another_flushbar/flushbar.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:kt_dart/kt.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:routines_app/application/workout/providers.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:routines_app/domain/workout/workout_failure.dart';
import 'package:routines_app/presentation/workout/widgets/add_new_workout_button.dart';
import 'package:routines_app/presentation/workout/widgets/critical_failure_page.dart';
import 'package:routines_app/presentation/workout/widgets/error_workout_card.dart';
import 'package:routines_app/presentation/workout/widgets/workout_card.dart';

final errorDeleteProvider =
    StateProvider<Either<WorkoutFailure, Unit>>((ref) => Right(unit));

class WorkoutOverviewPage extends StatelessWidget {
  const WorkoutOverviewPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          WorkoutsOverviewBody(),
          AddNewWorkoutButton(),
        ],
      ),
    );
  }
}

class WorkoutsOverviewBody extends ConsumerWidget {
  const WorkoutsOverviewBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    AsyncValue<Either<WorkoutFailure, KtList<Workout>>> workouts =
        ref.watch(workoutProvider);

    ref.listen<Either<WorkoutFailure, Unit>>(errorDeleteProvider,
        (previous, next) {
      next.fold(
        (failure) {
          return Flushbar(
            duration: Duration(seconds: 3),
            title: failure.map(
                // Use localized strings here in your apps
                insufficientPermission: (_) => 'Insufficient permissions ❌',
                unableToUpdate: (_) => 'Impossible error',
                unexpected: (_) =>
                    'Unexpected error occured while deleting, please contact support.'),
          ).show(context);
        },
        (_) {},
      );
    });

    return workouts.when(
      loading: () => Center(
        child: LoadingIndicator(
          indicatorType: Indicator.pacman,
          colors: [Colors.yellow[700]!, Colors.yellow[600]!],
        ),
      ),
      data: (data) => data.fold(
          (failure) => CriticalFailureDisplay(failure: failure), (workouts) {
        if (workouts.isNotEmpty()) {
          return ListView.builder(
            shrinkWrap: true,
            itemCount: workouts.size,
            itemBuilder: (context, index) {
              final workout = workouts[index];
              if (workout.failureOption.isSome())
                return ErrorWorkoutCard(workout: workout);
              else
                return WorkoutCard(workout: workout);
            },
          );
        } else
          return Container();
      }),
      error: (error, stackTrace) => Center(child: Text('$error')),
    );
  }
}

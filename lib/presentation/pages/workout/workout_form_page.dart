import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/application/workout/providers.dart';
import 'package:routines_app/domain/workout/workout_failure.dart';
import 'package:routines_app/presentation/workout/widgets/exercise/exercise_form.dart';
import 'package:routines_app/presentation/workout/widgets/exercise/exercise_tab_widget.dart';

class WorkoutFormPage extends ConsumerWidget {
  const WorkoutFormPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final exercises = ref.watch(workoutFormStateProvider).exercisePrimitives;
    return DefaultTabController(
      initialIndex: 0,
      length: exercises.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            ref.watch(workoutFormStateProvider.select((_) => _.isEditing))
                ? 'editWorkout'
                : 'createWorkout',
          ).tr(),
          actions: [
            IconButton(
              icon: Icon(Icons.check),
              onPressed: () async {
                //ref.read(workoutFromStateProvider).save();
              },
            ),
          ],
          bottom: TabBar(
            tabs: exercises
                .map((e) => ExerciseTab(
                      exercisePrimitive: e,
                    ))
                .toList(),
          ),
        ),
        body: TabBarView(
          children: exercises
              .map((e) => ExerciseForm(
                    initial: e,
                  ))
              .toList(),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.orange,
          child: Icon(
            Icons.add,
            color: Colors.orange[100],
          ),
          onPressed: () async {
            ref
                .read(workoutFormStateProvider.notifier)
                .addExercise('${exercises.length + 1}');
          },
        ),
      ),
    );
  }

  Future<dynamic> _showError(WorkoutFailure saveFailure, BuildContext context) {
    return FlushbarHelper.createError(
        duration: const Duration(seconds: 5),
        message: saveFailure.map(
          unexpected: (_) => 'unexpected'.tr(),
          insufficientPermission: (_) => 'insufficientPermission'.tr(),
          unableToUpdate: (_) => 'unableToUpdate'.tr(),
        )).show(context);
  }
}

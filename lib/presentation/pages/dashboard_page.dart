import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/infrastructure/auth/auth_service.dart';
import 'package:routines_app/presentation/pages/initial_page.dart';
import 'package:routines_app/presentation/routes/router.gr.dart';

class DashboardPage extends ConsumerWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref
        .watch(signedUserProvider)
        .fold(() => context.router.replace(const SignInRoute()), (_) {});

    return Dashboard();
  }
}

class Dashboard extends ConsumerWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              onTap: () {
                final router =
                    context.innerRouterOf<StackRouter>(DashboardRoute.name);
                context.router.pop();
                router?.pop();
                router?.push(const WorkoutOverviewRoute(),
                    onFailure: (f) => print(f.toString()));
              },
              leading: Icon(Icons.water),
            ),
            ListTile(
              onTap: () {
                final router =
                    context.innerRouterOf<StackRouter>(DashboardRoute.name);
                context.router.pop();
                router?.pop();
                router?.push(const PlaceHolderRoute(),
                    onFailure: (f) => print(f.toString()));
              },
              leading: Icon(Icons.document_scanner),
            ),
          ],
        ),
      ),
      appBar: AppBar(
        elevation: 0,
        title: const Text('Routines'),
        actions: [
          IconButton(
              onPressed: () {
                ref.read(AuthService.provider).signOut();
                ref.refresh(signedUserProvider);
              },
              icon: Icon(Icons.logout)),
        ],
      ),
      body: Container(
        child: AutoRouter(),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:auto_route/auto_route.dart';
import 'package:routines_app/infrastructure/auth/auth_service.dart';
import 'package:routines_app/presentation/routes/router.gr.dart';

final signedUserProvider = Provider.autoDispose(
    (ref) => ref.watch(AuthService.provider).getSignedInUser());

class InitialPage extends ConsumerWidget {
  const InitialPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.watch(signedUserProvider).fold(
        () => AutoRouter.of(context).replace(const SignInRoute()),
        (user) => AutoRouter.of(context).replace(const DashboardRoute()));

    return const Scaffold(
      body: Center(child: CircularProgressIndicator()),
    );
  }
}

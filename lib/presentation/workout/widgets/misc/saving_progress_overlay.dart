import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';
import 'package:easy_localization/easy_localization.dart';

class SavingProgressOverlay extends StatelessWidget {
  const SavingProgressOverlay({Key? key, required this.isSaving})
      : super(key: key);

  final bool isSaving;

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: !isSaving,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 150),
        color: isSaving ? Colors.black.withOpacity(.8) : Colors.transparent,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Visibility(
          visible: isSaving,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LoadingIndicator(
                indicatorType: Indicator.pacman,
                colors: [Colors.yellow[700]!, Colors.yellow[600]!],
              ),
              const SizedBox(height: 8),
              Text(
                'saving',
                style: Theme.of(context).textTheme.bodyText2!.copyWith(
                      color: Colors.white,
                      fontSize: 16,
                    ),
              ).tr(),
            ],
          ),
        ),
      ),
    );
  }
}

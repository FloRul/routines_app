import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:routines_app/domain/workout/workout.dart';

class ErrorWorkoutCard extends StatelessWidget {
  final Workout workout;
  const ErrorWorkoutCard({Key? key, required this.workout}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Theme.of(context).errorColor,
      child: Padding(
        padding: const EdgeInsets.all(4),
        child: Column(
          children: [
            Text(
              'invalidWorkoutCard',
              style: Theme.of(context)
                  .primaryTextTheme
                  .bodyText2
                  ?.copyWith(fontSize: 18),
            ).tr(),
            const SizedBox(
              height: 3,
            ),
            Text(
              'errorDetails',
              style: Theme.of(context).primaryTextTheme.bodyText2,
            ).tr(
              args: [
                '${workout.failureOption.fold(() => '', (f) => f.toString())}'
              ],
            ),
          ],
        ),
      ),
    );
  }
}

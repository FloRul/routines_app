import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/application/workout/providers.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:routines_app/presentation/pages/workout/workout_overview_page.dart';
import 'package:routines_app/presentation/routes/router.gr.dart';

class WorkoutCard extends ConsumerWidget {
  final Workout workout;
  const WorkoutCard({Key? key, required this.workout}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Slidable(
      actionPane: SlidableScrollActionPane(),
      actionExtentRatio: 0.25,
      secondaryActions: [
        IconSlideAction(
          caption: 'delete'.tr().toUpperCase(),
          color: Colors.red,
          icon: Icons.delete,
          onTap: () async {
            ref.read(errorDeleteProvider.state).state =
                await ref.read(workoutRepo).delete(workout);
          },
        )
      ],
      child: Card(
        child: InkWell(
          onTap: () async {
            ref.read(workoutFormStateProvider.notifier).setWorkout(workout);
            AutoRouter.of(context).root.push(WorkoutFormRoute());
          },
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              workout.name.getOrThrow(),
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:routines_app/presentation/misc/exercises_presentation_classes.dart';

class ExerciseTab extends StatelessWidget {
  final ExercisePrimitive exercisePrimitive;
  const ExerciseTab({Key? key, required this.exercisePrimitive})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: CircleAvatar(
        backgroundColor: Colors.orange[600],
        child: Text(
          this.exercisePrimitive.name,
          style: Theme.of(context)
              .textTheme
              .subtitle1
              ?.copyWith(color: Colors.white),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

const volumes = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65];

class VolumeField extends HookWidget {
  final Function(int)? onSelectedItemChanged;
  final int initialIndex;

  const VolumeField({
    Key? key,
    required this.onSelectedItemChanged,
    this.initialIndex = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const double extent = 60;
    final controller =
        useScrollController(initialScrollOffset: initialIndex * extent);
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 100,
            child: ListWheelScrollView(
              controller: controller,
              physics: FixedExtentScrollPhysics(),
              diameterRatio: 1.5,
              offAxisFraction: -.3,
              itemExtent: extent,
              onSelectedItemChanged: (_) {},
              children: volumes
                  .map((e) => Container(
                      padding: const EdgeInsets.all(8),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: Colors.red[300],
                          borderRadius: BorderRadius.circular(5)),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(e.toString()),
                      )))
                  .toList(),
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: Text('LBS'),
          ),
        ),
      ],
    );
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class NameField extends HookConsumerWidget {
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;
  final String? initialValue;
  final int maxLength;

  const NameField({
    Key? key,
    this.initialValue,
    this.validator,
    required this.onChanged,
    required this.maxLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final textController = useTextEditingController(text: initialValue);
    return Card(
      elevation: 10,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: TextFormField(
          controller: textController,
          decoration: InputDecoration(
            labelText: 'exerciseName'.tr(),
            counterText: '',
          ),
          maxLength: maxLength,
          maxLines: 1,
          minLines: 1,
          // le onchanged doit changer le modele du domaine
          onChanged: onChanged,
          // la validation se fait a partir des exceptions du modele
          validator: validator,
        ),
      ),
    );
  }
}

// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/application/workout/providers.dart';
import 'package:routines_app/presentation/misc/exercises_presentation_classes.dart';
import 'package:routines_app/presentation/workout/widgets/exercise/name_field.dart';
import 'package:routines_app/presentation/workout/widgets/exercise/volume_field.dart';

class ExerciseForm extends ConsumerWidget {
  const ExerciseForm({Key? key, required this.initial}) : super(key: key);
  final ExercisePrimitive initial;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Column(
        children: [
          NameField(
              initialValue: initial.name,
              validator: (p0) {},
              onChanged: (value) async => ref
                  .read(workoutFormStateProvider.notifier)
                  .updateName(value, initial.id),
              maxLength: 1),
          SizedBox(
            height: 10,
          ),
          VolumeField(
            onSelectedItemChanged: (item) async => ref
                .read(workoutFormStateProvider.notifier)
                .updateVolume(item, initial.id),
          ),
          SizedBox(
            width: 10,
          )
        ],
      ),
    );
  }
}

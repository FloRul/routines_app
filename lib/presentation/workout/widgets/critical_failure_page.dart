import 'package:flutter/material.dart';
import 'package:routines_app/domain/workout/workout_failure.dart';
import 'package:easy_localization/easy_localization.dart';

class CriticalFailureDisplay extends StatelessWidget {
  final WorkoutFailure failure;
  const CriticalFailureDisplay({Key? key, required this.failure})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var failureText = failure.map(
      unexpected: (_) => tr('unexpectedError'),
      insufficientPermission: (_) => tr('insufficientPermission'),
      unableToUpdate: (_) => tr('unableToUpdate'),
    );
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              'criticalError',
              style:
                  Theme.of(context).textTheme.headline1!.copyWith(fontSize: 30),
              textAlign: TextAlign.center,
            ).tr(
              args: [failureText],
            ),
          ),
          ElevatedButton(
              onPressed: () {
                //TODO: send support email
              },
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(Icons.mail),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    'mailToSupport',
                    style: TextStyle(color: Colors.white),
                  ).tr(),
                  SizedBox(
                    width: 8,
                  ),
                  Icon(Icons.exit_to_app)
                ],
              ))
        ],
      ),
    );
  }
}

import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/application/workout/providers.dart';
import 'package:routines_app/presentation/routes/router.gr.dart';

class AddNewWorkoutButton extends ConsumerWidget {
  const AddNewWorkoutButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12),
      child: ElevatedButton(
        onPressed: () async {
          ref.read(workoutFormStateProvider.notifier).setWorkout();
          AutoRouter.of(context).root.push(WorkoutFormRoute());
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(Icons.add),
            SizedBox(width: 8),
            Text('newWorkout').tr(),
          ],
        ),
      ),
    );
  }
}

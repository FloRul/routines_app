// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'exercises_presentation_classes.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$ExercisePrimitiveTearOff {
  const _$ExercisePrimitiveTearOff();

  _ExercisePrimitive call(
      {required UniqueId id,
      required String name,
      required String volume,
      required int restDuration}) {
    return _ExercisePrimitive(
      id: id,
      name: name,
      volume: volume,
      restDuration: restDuration,
    );
  }
}

/// @nodoc
const $ExercisePrimitive = _$ExercisePrimitiveTearOff();

/// @nodoc
mixin _$ExercisePrimitive {
  UniqueId get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  String get volume => throw _privateConstructorUsedError;
  int get restDuration => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ExercisePrimitiveCopyWith<ExercisePrimitive> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ExercisePrimitiveCopyWith<$Res> {
  factory $ExercisePrimitiveCopyWith(
          ExercisePrimitive value, $Res Function(ExercisePrimitive) then) =
      _$ExercisePrimitiveCopyWithImpl<$Res>;
  $Res call({UniqueId id, String name, String volume, int restDuration});
}

/// @nodoc
class _$ExercisePrimitiveCopyWithImpl<$Res>
    implements $ExercisePrimitiveCopyWith<$Res> {
  _$ExercisePrimitiveCopyWithImpl(this._value, this._then);

  final ExercisePrimitive _value;
  // ignore: unused_field
  final $Res Function(ExercisePrimitive) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? volume = freezed,
    Object? restDuration = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as UniqueId,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      volume: volume == freezed
          ? _value.volume
          : volume // ignore: cast_nullable_to_non_nullable
              as String,
      restDuration: restDuration == freezed
          ? _value.restDuration
          : restDuration // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
abstract class _$ExercisePrimitiveCopyWith<$Res>
    implements $ExercisePrimitiveCopyWith<$Res> {
  factory _$ExercisePrimitiveCopyWith(
          _ExercisePrimitive value, $Res Function(_ExercisePrimitive) then) =
      __$ExercisePrimitiveCopyWithImpl<$Res>;
  @override
  $Res call({UniqueId id, String name, String volume, int restDuration});
}

/// @nodoc
class __$ExercisePrimitiveCopyWithImpl<$Res>
    extends _$ExercisePrimitiveCopyWithImpl<$Res>
    implements _$ExercisePrimitiveCopyWith<$Res> {
  __$ExercisePrimitiveCopyWithImpl(
      _ExercisePrimitive _value, $Res Function(_ExercisePrimitive) _then)
      : super(_value, (v) => _then(v as _ExercisePrimitive));

  @override
  _ExercisePrimitive get _value => super._value as _ExercisePrimitive;

  @override
  $Res call({
    Object? id = freezed,
    Object? name = freezed,
    Object? volume = freezed,
    Object? restDuration = freezed,
  }) {
    return _then(_ExercisePrimitive(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as UniqueId,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      volume: volume == freezed
          ? _value.volume
          : volume // ignore: cast_nullable_to_non_nullable
              as String,
      restDuration: restDuration == freezed
          ? _value.restDuration
          : restDuration // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_ExercisePrimitive extends _ExercisePrimitive {
  const _$_ExercisePrimitive(
      {required this.id,
      required this.name,
      required this.volume,
      required this.restDuration})
      : super._();

  @override
  final UniqueId id;
  @override
  final String name;
  @override
  final String volume;
  @override
  final int restDuration;

  @override
  String toString() {
    return 'ExercisePrimitive(id: $id, name: $name, volume: $volume, restDuration: $restDuration)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _ExercisePrimitive &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.volume, volume) || other.volume == volume) &&
            (identical(other.restDuration, restDuration) ||
                other.restDuration == restDuration));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, name, volume, restDuration);

  @JsonKey(ignore: true)
  @override
  _$ExercisePrimitiveCopyWith<_ExercisePrimitive> get copyWith =>
      __$ExercisePrimitiveCopyWithImpl<_ExercisePrimitive>(this, _$identity);
}

abstract class _ExercisePrimitive extends ExercisePrimitive {
  const factory _ExercisePrimitive(
      {required UniqueId id,
      required String name,
      required String volume,
      required int restDuration}) = _$_ExercisePrimitive;
  const _ExercisePrimitive._() : super._();

  @override
  UniqueId get id;
  @override
  String get name;
  @override
  String get volume;
  @override
  int get restDuration;
  @override
  @JsonKey(ignore: true)
  _$ExercisePrimitiveCopyWith<_ExercisePrimitive> get copyWith =>
      throw _privateConstructorUsedError;
}

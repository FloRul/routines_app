import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';
import 'package:routines_app/domain/core/value_objects.dart';
import 'package:routines_app/domain/workout/workout.dart';
part 'exercises_presentation_classes.freezed.dart';

class FormExercises extends ValueNotifier<KtList<ExercisePrimitive>> {
  FormExercises() : super(emptyList<ExercisePrimitive>());
}

// [ExercisePrimitive] Used so we can pass a non validated object into the event we pass to the bloc.
@freezed
class ExercisePrimitive with _$ExercisePrimitive {
  const ExercisePrimitive._();
  const factory ExercisePrimitive({
    required UniqueId id,
    required String name,
    required String volume,
    required int restDuration,
  }) = _ExercisePrimitive;

  factory ExercisePrimitive.empty() => ExercisePrimitive(
        id: UniqueId(),
        name: '',
        volume: '',
        restDuration: 0,
      );

  factory ExercisePrimitive.emptyWithDefaultName(String name) =>
      ExercisePrimitive(
        id: UniqueId(),
        name: name,
        volume: '',
        restDuration: 0,
      );

  factory ExercisePrimitive.fromDomain(Exercise exercise) => ExercisePrimitive(
        id: exercise.id,
        // If we somehow get to this point, we missed something in other parts of the UI. It's better to throw an Error.
        name: exercise.name.getOrThrow(),
        volume: exercise.volume.getOrThrow().fold((d) => 'd$d', (r) => 'r$r'),
        restDuration: exercise.restDuration.getOrThrow(),
      );

  Exercise toDomain() {
    return Exercise(
      id: id,
      name: Name(name),
      restDuration: RestDuration(restDuration),
      volume: Volume.fromString(volume),
    );
  }
}

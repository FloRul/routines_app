import 'package:auto_route/annotations.dart';
import 'package:routines_app/presentation/pages/placeholder_page.dart';
import 'package:routines_app/presentation/pages/dashboard_page.dart';
import 'package:routines_app/presentation/pages/initial_page.dart';
import 'package:routines_app/presentation/pages/workout/workout_form_page.dart';
import 'package:routines_app/presentation/pages/workout/workout_overview_page.dart';
import 'package:routines_app/presentation/sign_in/sign_in_page.dart';

@MaterialAutoRouter(replaceInRouteName: 'Page,Route', routes: <AutoRoute>[
  AutoRoute(page: InitialPage, initial: true),
  AutoRoute(page: SignInPage),
  AutoRoute(page: WorkoutFormPage),
  AutoRoute(
    page: DashboardPage,
    children: [
      AutoRoute(page: WorkoutOverviewPage),
      AutoRoute(page: PlaceHolderPage),
    ],
  ),
])
class $AppRouter {}

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:auto_route/auto_route.dart' as _i7;
import 'package:flutter/material.dart' as _i8;

import '../pages/dashboard_page.dart' as _i4;
import '../pages/initial_page.dart' as _i1;
import '../pages/placeholder_page.dart' as _i6;
import '../pages/workout/workout_form_page.dart' as _i3;
import '../pages/workout/workout_overview_page.dart' as _i5;
import '../sign_in/sign_in_page.dart' as _i2;

class AppRouter extends _i7.RootStackRouter {
  AppRouter([_i8.GlobalKey<_i8.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i7.PageFactory> pagesMap = {
    InitialRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.InitialPage());
    },
    SignInRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.SignInPage());
    },
    WorkoutFormRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i3.WorkoutFormPage());
    },
    DashboardRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i4.DashboardPage());
    },
    WorkoutOverviewRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i5.WorkoutOverviewPage());
    },
    PlaceHolderRoute.name: (routeData) {
      return _i7.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i6.PlaceHolderPage());
    }
  };

  @override
  List<_i7.RouteConfig> get routes => [
        _i7.RouteConfig(InitialRoute.name, path: '/'),
        _i7.RouteConfig(SignInRoute.name, path: '/sign-in-page'),
        _i7.RouteConfig(WorkoutFormRoute.name, path: '/workout-form-page'),
        _i7.RouteConfig(DashboardRoute.name,
            path: '/dashboard-page',
            children: [
              _i7.RouteConfig(WorkoutOverviewRoute.name,
                  path: 'workout-overview-page', parent: DashboardRoute.name),
              _i7.RouteConfig(PlaceHolderRoute.name,
                  path: 'place-holder-page', parent: DashboardRoute.name)
            ])
      ];
}

/// generated route for [_i1.InitialPage]
class InitialRoute extends _i7.PageRouteInfo<void> {
  const InitialRoute() : super(name, path: '/');

  static const String name = 'InitialRoute';
}

/// generated route for [_i2.SignInPage]
class SignInRoute extends _i7.PageRouteInfo<void> {
  const SignInRoute() : super(name, path: '/sign-in-page');

  static const String name = 'SignInRoute';
}

/// generated route for [_i3.WorkoutFormPage]
class WorkoutFormRoute extends _i7.PageRouteInfo<void> {
  const WorkoutFormRoute() : super(name, path: '/workout-form-page');

  static const String name = 'WorkoutFormRoute';
}

/// generated route for [_i4.DashboardPage]
class DashboardRoute extends _i7.PageRouteInfo<void> {
  const DashboardRoute({List<_i7.PageRouteInfo>? children})
      : super(name, path: '/dashboard-page', initialChildren: children);

  static const String name = 'DashboardRoute';
}

/// generated route for [_i5.WorkoutOverviewPage]
class WorkoutOverviewRoute extends _i7.PageRouteInfo<void> {
  const WorkoutOverviewRoute() : super(name, path: 'workout-overview-page');

  static const String name = 'WorkoutOverviewRoute';
}

/// generated route for [_i6.PlaceHolderPage]
class PlaceHolderRoute extends _i7.PageRouteInfo<void> {
  const PlaceHolderRoute() : super(name, path: 'place-holder-page');

  static const String name = 'PlaceHolderRoute';
}

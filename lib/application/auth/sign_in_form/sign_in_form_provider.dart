import 'dart:async';
import 'package:routines_app/application/auth/sign_in_form/sign_in_form_state.dart';
import 'package:dartz/dartz.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/domain/auth/auth_failure.dart';
import 'package:routines_app/domain/auth/email_address.dart';
import 'package:routines_app/domain/auth/password.dart';
import 'package:routines_app/infrastructure/auth/auth_service.dart';

final signInStateProvider =
    StateNotifierProvider<SignInFormNotifier, SignInFormState>(
        (ref) => SignInFormNotifier());

class SignInFormNotifier extends StateNotifier<SignInFormState> {
  SignInFormNotifier() : super(SignInFormState.initial());

  Future<void> signInWithEmailAndPassword(Reader read) async {
    Either<AuthFailure, Unit> failureOrSuccess = right(unit);

    if (state.emailAddress.isValid() && state.password.isValid()) {
      state = state.copyWith(
        isSubmitting: true,
        authFailureOrSuccessOption: none(),
      );

      failureOrSuccess =
          await read(AuthService.provider).signInWithEmailAndPassword(
        emailAddress: state.emailAddress,
        password: state.password,
      );
    }

    state = state.copyWith(
      isSubmitting: false,
      showErrorMessage: true,
      authFailureOrSuccessOption: optionOf(failureOrSuccess),
    );
  }

  Future<void> registerWithEmailAndPassword(Reader read) async {
    Either<AuthFailure, Unit> failureOrSuccess = right(unit);

    if (state.emailAddress.isValid() && state.password.isValid()) {
      state = state.copyWith(
        isSubmitting: true,
        authFailureOrSuccessOption: none(),
      );

      failureOrSuccess =
          await read(AuthService.provider).registerWithEmailAndPassword(
        emailAddress: state.emailAddress,
        password: state.password,
      );
    }

    state = state.copyWith(
      isSubmitting: false,
      showErrorMessage: true,
      authFailureOrSuccessOption: optionOf(failureOrSuccess),
    );
  }

  set password(String value) => state = state.copyWith(
        password: Password(value),
        authFailureOrSuccessOption: none(),
      );

  set email(String value) => state = state.copyWith(
        emailAddress: EmailAddress(value),
        authFailureOrSuccessOption: none(),
      );

  Future<void> signInWithGoogle(Reader read) async {
    state.copyWith(
      isSubmitting: true,
      authFailureOrSuccessOption: none(),
    );
    final failureOrSuccess =
        await read(AuthService.provider).signInWithGoogle();
    state = state.copyWith(
        isSubmitting: false,
        authFailureOrSuccessOption: some(failureOrSuccess));
  }
}

// @injectable
// class SignInFormBloc extends Bloc<SignInFormEvent, SignInFormState> {
//   final IAuthFacade _authFacade;
//   SignInFormBloc(this._authFacade) : super(SignInFormState.initial());

//   Stream<SignInFormState> _performActionOnAuthFacadeWithEmailAndPassword(
//     Future<Either<AuthFailure, Unit>> Function({
//       required EmailAddress emailAddress,
//       required Password password,
//     })
//         forwardedCall,
//   ) async* {
//     Either<AuthFailure, Unit> failureOrSuccess = right(unit);

//     if (state.emailAddress.isValid() && state.password.isValid()) {
//       yield state.copyWith(
//         isSubmitting: true,
//         authFailureOrSuccessOption: none(),
//       );

//       failureOrSuccess = await forwardedCall(
//         emailAddress: state.emailAddress,
//         password: state.password,
//       );
//     }

//     yield state.copyWith(
//       isSubmitting: false,
//       showErrorMessage: true,
//       authFailureOrSuccessOption: optionOf(failureOrSuccess),
//     );
//   }

//   @override
//   Stream<SignInFormState> mapEventToState(
//     SignInFormEvent event,
//   ) async* {
//     yield* event.map(
//       emailChanged: (e) async* {
//         yield state.copyWith(
//           emailAddress: EmailAddress(e.emailStr),
//           authFailureOrSuccessOption: none(),
//         );
//       },
//       passwordChanged: (e) async* {
//         yield state.copyWith(
//             password: Password(e.passwordStr),
//             authFailureOrSuccessOption: none());
//       },
//       registerWithEmailAndPassword: (e) async* {
//         yield* _performActionOnAuthFacadeWithEmailAndPassword(
//             _authFacade.registerWithEmailAndPassword);
//       },
//       signInWithEmailAndPassword: (e) async* {
//         yield* _performActionOnAuthFacadeWithEmailAndPassword(
//             _authFacade.signInWithEmailAndPassword);
//       },
//       signInWithGoogle: (e) async* {
//         yield state.copyWith(
//           isSubmitting: true,
//           authFailureOrSuccessOption: none(),
//         );
//         final failureOrSuccess = await _authFacade.signInWithGoogle();
//         yield state.copyWith(
//             isSubmitting: false,
//             authFailureOrSuccessOption: some(failureOrSuccess));
//       },
//     );
//   }
// }

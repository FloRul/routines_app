import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:routines_app/domain/workout/workout_failure.dart';
import 'package:routines_app/presentation/misc/exercises_presentation_classes.dart';

part 'workout_form_state.freezed.dart';

@freezed
class WorkoutFormState with _$WorkoutFormState {
  const factory WorkoutFormState({
    required Workout workout,
    required List<ExercisePrimitive> exercisePrimitives,
    required bool showErrorMessage,
    required bool isEditing,
    required bool isSaving,
    // represent the state of the saving, either we are in a default state, or we are either (in a failure state when we tried to save or we successfully saved)
    required Option<Either<WorkoutFailure, Unit>> saveFailureOrSuccessOption,
  }) = _WorkoutFormState;

  factory WorkoutFormState.initial() => WorkoutFormState(
        workout: Workout.empty(),
        exercisePrimitives: [],
        showErrorMessage: false,
        isEditing: false,
        isSaving: false,
        saveFailureOrSuccessOption: none(),
      );

  factory WorkoutFormState.fromDomain(Workout workout) => WorkoutFormState(
        workout: workout,
        exercisePrimitives: workout.exercises
            .getOrThrow()
            .iter
            .map((e) => ExercisePrimitive.fromDomain(e))
            .toList(),
        isEditing: true,
        showErrorMessage: false,
        isSaving: false,
        saveFailureOrSuccessOption: none(),
      );
}

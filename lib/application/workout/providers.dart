import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:routines_app/application/workout/workout_form_state.dart';
import 'package:routines_app/domain/core/value_objects.dart';
import 'package:routines_app/domain/workout/workout.dart';
import 'package:routines_app/infrastructure/workout/workout_repository.dart';
import 'package:routines_app/presentation/misc/exercises_presentation_classes.dart';

final workoutRepo = Provider((ref) => WorkoutRepository());

final workoutProvider = StreamProvider.autoDispose((ref) {
  final stream = ref.read(workoutRepo).watchAll();
  return stream;
});

final workoutFormStateProvider =
    StateNotifierProvider<WorkoutForm, WorkoutFormState>((ref) {
  return WorkoutForm();
});

class WorkoutForm extends StateNotifier<WorkoutFormState> {
  WorkoutForm() : super(WorkoutFormState.initial());

  void setWorkout([Workout? workout]) => state = workout != null
      ? WorkoutFormState.fromDomain(workout)
      : WorkoutFormState.initial();

  void addExercise(String name) => state = state.copyWith(exercisePrimitives: [
        ...state.exercisePrimitives,
        ExercisePrimitive.emptyWithDefaultName(name),
      ]);

  void updateName(String value, UniqueId id) => state = state.copyWith(
        exercisePrimitives: [
          for (final e in state.exercisePrimitives)
            if (e.id == id)
              ExercisePrimitive(
                id: e.id,
                name: value,
                restDuration: e.restDuration,
                volume: e.volume,
              )
        ],
      );

  void updateVolume(int volume, UniqueId id) => state = state.copyWith(
        exercisePrimitives: [
          for (final e in state.exercisePrimitives)
            if (e.id == id)
              ExercisePrimitive(
                id: e.id,
                name: e.name,
                restDuration: e.restDuration,
                volume: volume.toString(),
              )
        ],
      );
}

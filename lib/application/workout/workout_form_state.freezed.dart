// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'workout_form_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$WorkoutFormStateTearOff {
  const _$WorkoutFormStateTearOff();

  _WorkoutFormState call(
      {required Workout workout,
      required List<ExercisePrimitive> exercisePrimitives,
      required bool showErrorMessage,
      required bool isEditing,
      required bool isSaving,
      required Option<Either<WorkoutFailure, Unit>>
          saveFailureOrSuccessOption}) {
    return _WorkoutFormState(
      workout: workout,
      exercisePrimitives: exercisePrimitives,
      showErrorMessage: showErrorMessage,
      isEditing: isEditing,
      isSaving: isSaving,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption,
    );
  }
}

/// @nodoc
const $WorkoutFormState = _$WorkoutFormStateTearOff();

/// @nodoc
mixin _$WorkoutFormState {
  Workout get workout => throw _privateConstructorUsedError;
  List<ExercisePrimitive> get exercisePrimitives =>
      throw _privateConstructorUsedError;
  bool get showErrorMessage => throw _privateConstructorUsedError;
  bool get isEditing => throw _privateConstructorUsedError;
  bool get isSaving =>
      throw _privateConstructorUsedError; // represent the state of the saving, either we are in a default state, or we are either (in a failure state when we tried to save or we successfully saved)
  Option<Either<WorkoutFailure, Unit>> get saveFailureOrSuccessOption =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WorkoutFormStateCopyWith<WorkoutFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WorkoutFormStateCopyWith<$Res> {
  factory $WorkoutFormStateCopyWith(
          WorkoutFormState value, $Res Function(WorkoutFormState) then) =
      _$WorkoutFormStateCopyWithImpl<$Res>;
  $Res call(
      {Workout workout,
      List<ExercisePrimitive> exercisePrimitives,
      bool showErrorMessage,
      bool isEditing,
      bool isSaving,
      Option<Either<WorkoutFailure, Unit>> saveFailureOrSuccessOption});

  $WorkoutCopyWith<$Res> get workout;
}

/// @nodoc
class _$WorkoutFormStateCopyWithImpl<$Res>
    implements $WorkoutFormStateCopyWith<$Res> {
  _$WorkoutFormStateCopyWithImpl(this._value, this._then);

  final WorkoutFormState _value;
  // ignore: unused_field
  final $Res Function(WorkoutFormState) _then;

  @override
  $Res call({
    Object? workout = freezed,
    Object? exercisePrimitives = freezed,
    Object? showErrorMessage = freezed,
    Object? isEditing = freezed,
    Object? isSaving = freezed,
    Object? saveFailureOrSuccessOption = freezed,
  }) {
    return _then(_value.copyWith(
      workout: workout == freezed
          ? _value.workout
          : workout // ignore: cast_nullable_to_non_nullable
              as Workout,
      exercisePrimitives: exercisePrimitives == freezed
          ? _value.exercisePrimitives
          : exercisePrimitives // ignore: cast_nullable_to_non_nullable
              as List<ExercisePrimitive>,
      showErrorMessage: showErrorMessage == freezed
          ? _value.showErrorMessage
          : showErrorMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      isEditing: isEditing == freezed
          ? _value.isEditing
          : isEditing // ignore: cast_nullable_to_non_nullable
              as bool,
      isSaving: isSaving == freezed
          ? _value.isSaving
          : isSaving // ignore: cast_nullable_to_non_nullable
              as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<WorkoutFailure, Unit>>,
    ));
  }

  @override
  $WorkoutCopyWith<$Res> get workout {
    return $WorkoutCopyWith<$Res>(_value.workout, (value) {
      return _then(_value.copyWith(workout: value));
    });
  }
}

/// @nodoc
abstract class _$WorkoutFormStateCopyWith<$Res>
    implements $WorkoutFormStateCopyWith<$Res> {
  factory _$WorkoutFormStateCopyWith(
          _WorkoutFormState value, $Res Function(_WorkoutFormState) then) =
      __$WorkoutFormStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Workout workout,
      List<ExercisePrimitive> exercisePrimitives,
      bool showErrorMessage,
      bool isEditing,
      bool isSaving,
      Option<Either<WorkoutFailure, Unit>> saveFailureOrSuccessOption});

  @override
  $WorkoutCopyWith<$Res> get workout;
}

/// @nodoc
class __$WorkoutFormStateCopyWithImpl<$Res>
    extends _$WorkoutFormStateCopyWithImpl<$Res>
    implements _$WorkoutFormStateCopyWith<$Res> {
  __$WorkoutFormStateCopyWithImpl(
      _WorkoutFormState _value, $Res Function(_WorkoutFormState) _then)
      : super(_value, (v) => _then(v as _WorkoutFormState));

  @override
  _WorkoutFormState get _value => super._value as _WorkoutFormState;

  @override
  $Res call({
    Object? workout = freezed,
    Object? exercisePrimitives = freezed,
    Object? showErrorMessage = freezed,
    Object? isEditing = freezed,
    Object? isSaving = freezed,
    Object? saveFailureOrSuccessOption = freezed,
  }) {
    return _then(_WorkoutFormState(
      workout: workout == freezed
          ? _value.workout
          : workout // ignore: cast_nullable_to_non_nullable
              as Workout,
      exercisePrimitives: exercisePrimitives == freezed
          ? _value.exercisePrimitives
          : exercisePrimitives // ignore: cast_nullable_to_non_nullable
              as List<ExercisePrimitive>,
      showErrorMessage: showErrorMessage == freezed
          ? _value.showErrorMessage
          : showErrorMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      isEditing: isEditing == freezed
          ? _value.isEditing
          : isEditing // ignore: cast_nullable_to_non_nullable
              as bool,
      isSaving: isSaving == freezed
          ? _value.isSaving
          : isSaving // ignore: cast_nullable_to_non_nullable
              as bool,
      saveFailureOrSuccessOption: saveFailureOrSuccessOption == freezed
          ? _value.saveFailureOrSuccessOption
          : saveFailureOrSuccessOption // ignore: cast_nullable_to_non_nullable
              as Option<Either<WorkoutFailure, Unit>>,
    ));
  }
}

/// @nodoc

class _$_WorkoutFormState implements _WorkoutFormState {
  const _$_WorkoutFormState(
      {required this.workout,
      required this.exercisePrimitives,
      required this.showErrorMessage,
      required this.isEditing,
      required this.isSaving,
      required this.saveFailureOrSuccessOption});

  @override
  final Workout workout;
  @override
  final List<ExercisePrimitive> exercisePrimitives;
  @override
  final bool showErrorMessage;
  @override
  final bool isEditing;
  @override
  final bool isSaving;
  @override // represent the state of the saving, either we are in a default state, or we are either (in a failure state when we tried to save or we successfully saved)
  final Option<Either<WorkoutFailure, Unit>> saveFailureOrSuccessOption;

  @override
  String toString() {
    return 'WorkoutFormState(workout: $workout, exercisePrimitives: $exercisePrimitives, showErrorMessage: $showErrorMessage, isEditing: $isEditing, isSaving: $isSaving, saveFailureOrSuccessOption: $saveFailureOrSuccessOption)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _WorkoutFormState &&
            (identical(other.workout, workout) || other.workout == workout) &&
            const DeepCollectionEquality()
                .equals(other.exercisePrimitives, exercisePrimitives) &&
            (identical(other.showErrorMessage, showErrorMessage) ||
                other.showErrorMessage == showErrorMessage) &&
            (identical(other.isEditing, isEditing) ||
                other.isEditing == isEditing) &&
            (identical(other.isSaving, isSaving) ||
                other.isSaving == isSaving) &&
            (identical(other.saveFailureOrSuccessOption,
                    saveFailureOrSuccessOption) ||
                other.saveFailureOrSuccessOption ==
                    saveFailureOrSuccessOption));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      workout,
      const DeepCollectionEquality().hash(exercisePrimitives),
      showErrorMessage,
      isEditing,
      isSaving,
      saveFailureOrSuccessOption);

  @JsonKey(ignore: true)
  @override
  _$WorkoutFormStateCopyWith<_WorkoutFormState> get copyWith =>
      __$WorkoutFormStateCopyWithImpl<_WorkoutFormState>(this, _$identity);
}

abstract class _WorkoutFormState implements WorkoutFormState {
  const factory _WorkoutFormState(
      {required Workout workout,
      required List<ExercisePrimitive> exercisePrimitives,
      required bool showErrorMessage,
      required bool isEditing,
      required bool isSaving,
      required Option<Either<WorkoutFailure, Unit>>
          saveFailureOrSuccessOption}) = _$_WorkoutFormState;

  @override
  Workout get workout;
  @override
  List<ExercisePrimitive> get exercisePrimitives;
  @override
  bool get showErrorMessage;
  @override
  bool get isEditing;
  @override
  bool get isSaving;
  @override // represent the state of the saving, either we are in a default state, or we are either (in a failure state when we tried to save or we successfully saved)
  Option<Either<WorkoutFailure, Unit>> get saveFailureOrSuccessOption;
  @override
  @JsonKey(ignore: true)
  _$WorkoutFormStateCopyWith<_WorkoutFormState> get copyWith =>
      throw _privateConstructorUsedError;
}
